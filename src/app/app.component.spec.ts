import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { SimplGetComponent } from './simpl-get/simpl-get.component';
import { SimpleServiceService } from './simpl-get/simple-service.service';
import { HttpModule } from '@angular/http';

import { Observable } from "rxjs/Observable";
import 'rxjs/add/observable/of';

class MockSimpleService {
  simpleGetJsonAlreadyMapped() {
    return Observable.of(true);
  }
}

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        SimplGetComponent
      ],
      providers: [
        SimpleServiceService,
        { provide: SimpleServiceService, useClass: MockSimpleService }
      ],
      imports: [
        HttpModule,
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Hello');
  }));
});
