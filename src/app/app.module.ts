import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER } from '@angular/core';

import { AppComponent } from './app.component';
import { SimplGetComponent } from './simpl-get/simpl-get.component';
import { SimpleServiceService } from './simpl-get/simple-service.service';
import { AppConfigService } from './appconfig.service';

export function appConfigServiceFactory (config: AppConfigService) {
  return () => config.load()
}

@NgModule({
  declarations: [
    AppComponent,
    SimplGetComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
  ],
  providers: [
    SimpleServiceService,
    AppConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: appConfigServiceFactory,
      deps: [AppConfigService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
