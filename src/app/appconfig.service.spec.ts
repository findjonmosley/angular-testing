import { TestBed, inject } from '@angular/core/testing';
import { HttpModule } from "@angular/http";
import { HttpClientModule } from '@angular/common/http';

import { AppConfigService } from './appconfig.service';

describe('AppConfigService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppConfigService],
      imports: [
        HttpModule,
        HttpClientModule
      ]
    });
  });

  it('should be created', inject([AppConfigService], (service: AppConfigService) => {
    expect(service).toBeTruthy();
  }));
});
