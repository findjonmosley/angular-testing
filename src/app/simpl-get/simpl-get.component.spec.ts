import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpModule } from "@angular/http";

import { SimplGetComponent } from './simpl-get.component';
import { SimpleServiceService } from './simple-service.service';

import { Observable } from "rxjs/Observable";
import 'rxjs/add/observable/of';

class MockSimpleServ {
  simpleGetJsonAlreadyMapped(){
    return Observable.of([{name: "silly data dev"}])
  }
}

describe('SimplGetComponent', () => {
  let component: SimplGetComponent;
  let fixture: ComponentFixture<SimplGetComponent>;
  let service: SimpleServiceService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimplGetComponent ],
      providers: [
        { provide: SimpleServiceService, useClass: MockSimpleServ }
      ],
      imports: [
        HttpModule
      ]
    })
    .compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimplGetComponent);
    component = fixture.componentInstance;
    let el = fixture.debugElement;
    // fixture.detectChanges();
    service = el.injector.get(SimpleServiceService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should do the get', () => {
    spyOn(service, 'simpleGetJsonAlreadyMapped').and.callThrough();
    component.ngOnInit();
    service.simpleGetJsonAlreadyMapped()
      .subscribe((result) => {
        expect(result).toEqual([{name: "silly data dev"}]);
      })
    expect(service.simpleGetJsonAlreadyMapped).toHaveBeenCalled();
  });
});
