import { Component, OnInit } from '@angular/core';
import { SimpleServiceService } from './simple-service.service';

@Component({
  selector: 'app-simpl-get',
  templateUrl: './simpl-get.component.html',
  styleUrls: ['./simpl-get.component.css']
})
export class SimplGetComponent implements OnInit {

  constructor(private simplGet: SimpleServiceService) { }

  ngOnInit() {
    this.simplGet
      .simpleGetJsonAlreadyMapped()
      .subscribe(response => console.log(response, 'simpleGetJsonAlreadyMapped'))
  }

}
