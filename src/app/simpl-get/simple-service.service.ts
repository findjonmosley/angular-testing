import { Injectable } from '@angular/core';
import { Http, Response } from "@angular/http";
import { AppConfigService } from "../appconfig.service";

import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';

@Injectable()
export class SimpleServiceService {

  constructor(private http: Http, private appConfig: AppConfigService) { }

  simpleGetJsonAlreadyMapped(): Observable<any> {
    return this.http.get(`${this.appConfig.get('api').base_url}`)
      .map((response: Response) => {
        return response.json();
      })
  }

}
