import { TestBed, inject } from '@angular/core/testing';
import { Response, ResponseOptions, Http } from "@angular/http";

import { SimpleServiceService } from './simple-service.service';
import { AppConfigService } from '../appconfig.service';

import { Observable } from "rxjs/Observable";
import 'rxjs/add/observable/of';

function createResponse(body) {
  return Observable.of(
    new Response(new ResponseOptions({ body: JSON.stringify(body)}))
  )
}

class MockHttp {
  get() {
    return createResponse([]);
  }
}

class MockConfig {
  get() {
    return 'http://localhost:3000/api-development';
  }
}

const dev = [{name: "Silly response"}];

describe('SimpleServiceService', () => {
  let http: Http;
  let service: SimpleServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SimpleServiceService,
        AppConfigService,
        Http,
        { provide: Http, useClass: MockHttp },
        { provide: AppConfigService, useClass: MockConfig }
      ]
    });
    http = TestBed.get(Http);
    service = TestBed.get(SimpleServiceService)
  });

  it('should be created', inject([SimpleServiceService], (service: SimpleServiceService) => {
    expect(service).toBeTruthy();
  }));

  it('should get dev env', () => {
    spyOn(http, 'get').and.returnValue(createResponse(dev))

    service.simpleGetJsonAlreadyMapped()
      .subscribe((result) => {
        expect(result.length).toBe(1);
        expect(result).toEqual(dev);
      })
  })
});
